import { request } from 'express';
import postBook from '../model/book.js';


export const getBooks=async(request,response)=>{
    try {
        const books=await postBook.find();
        response.status(200).json(books)
    } catch (error) {
        response.status(404).json({message:error.message})
    }
}

export const getBookById=async(request,response)=>{
    try {
        const book=await postBook.findById(request.params.id);
            response.status(209).json(book);
    } catch (error) {
        response.status(404).json({message:error.message})     
    }
}