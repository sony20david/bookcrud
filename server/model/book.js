import mongoose from "mongoose";

const bookSchema=mongoose.Schema({
    bookname:String,
    book_description:String,
    book_price:Number,
    book_discount:String,
    book_availability:Number,
    book_author:String,
    book_publisher:String
})

const postBook = mongoose.model('book', bookSchema);

export default postBook;