import express from "express";
import {getBooks,getBookById} from '../controller/get_book.js';
import { addBook } from "../controller/post_book.js";
import { editBook } from "../controller/update_book.js";
import { deleteBook } from "../controller/delete_book.js";

const router=express.Router();

router.get('/',getBooks);
router.post('/add',addBook);
router.get('/:id',getBookById);
router.put('/:id',editBook);
router.delete('/:id',deleteBook);

export default router;