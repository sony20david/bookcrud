import axios from 'axios';

const booksUrl = 'http://localhost:4002/users';

export const getBooks = async (id) => {
    id = id || '';
    return await axios.get(`${booksUrl}/${id}`);
}

export const addBook = async (book) => {
    return await axios.post(`${booksUrl}/add`, book);
}

export const deleteBook = async (id) => {
    return await axios.delete(`${booksUrl}/${id}`);
}

export const editBook = async (id, book) => {
    return await axios.put(`${booksUrl}/${id}`, book)
}