import { AppBar, Toolbar, makeStyles } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import LibraryBooksIcon from '@mui/icons-material/LibraryBooks';
import HomeIcon from '@mui/icons-material/Home';


const useStyle = makeStyles({
    header: {
        background: '#BC789E',
        display:"flex",
        flexDirection:"row",
        justifyContent:"left"
    },
    tabs: {
        color: '#FFFFFF',
        marginRight: 20,
        textDecoration: 'none',
        fontSize: 20
    },
})



const NavBar = () => {
    const classes = useStyle();
    return (
        <AppBar position="static" className={classes.header}>
            <Toolbar sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                
                <NavLink className={classes.tabs} to="/" exact><LibraryBooksIcon sx={{ fontSize: 45}}/></NavLink>
                <NavLink className={classes.tabs} to="add" exact>Add Books</NavLink>
                <NavLink className={classes.tabs} to="all" exact>All Books</NavLink>
                <NavLink className={classes.tabs} to="card" exact>Books view</NavLink>
                {/* <h3  >Welcome to BOOK CATLOG</h3> */}
                
            </Toolbar>
        </AppBar>
        
    )
}

export default NavBar;