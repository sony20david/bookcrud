import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';

export default function MediaCard() {
  return (
   
    <Card sx={{ minWidth: 250,minHeight:"100%"}}  style={{backgroundColor:"#FAE7E3"}}>
      <CardMedia
        component="img"
        height="200px"
        image="./oho.jpg"
        alt="green iguana"
      />
      <CardContent >
        <Typography gutterBottom variant="h5" component="div">
        <CardActions>
        <Button size="small" component={Link} to={'./add'}>ADD BOOK</Button>
        
      </CardActions>
        </Typography>
        <Typography variant="body2" color="text.secondary">
        <CardActions>
        
        <Button size="small" component={Link} to={'./card'}>Book View</Button>
      </CardActions>
        </Typography>
        <Typography variant="body2" color="text.secondary">
        <CardActions>
        
        <Button size="small" component={Link} to={'./all'}>All Books</Button>
      </CardActions>
        </Typography>
      </CardContent>
      
    </Card>
    
  );
}
