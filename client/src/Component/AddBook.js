import react, { useState } from 'react';
import { FormGroup,FormHelperText, FormControl, InputLabel, Input, Button, makeStyles, Typography } from '@material-ui/core';
import { addBook } from '../Service/api';
import { useHistory } from 'react-router-dom';
import M from "materialize-css";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



import { validate_author_name, validate_book_name,validate_description, validate_discount, validate_price, validate_publisher_name, validate_stock, validate_type_name } from './validation';

toast.configure();

const initialValue = {
    bookname: '',
    book_description: '',
    book_price: '',
    book_discount: '',
    book_availability:'',
    book_author:'',
    book_publisher:'',
    book_type:'',
    booknameError:''
}


const useStyles = makeStyles({
    container: {
        width: '50%',
        margin: '5% 0 0 15%',
        '& > *': {
            marginTop: 20
        },
        backgroundColor:"#F8ECF2"
        
    },
    bg:{
        display:"flex",
    flexDirection:"row",
    flexwrap:"wrap",
    backgroundColor:"#F8ECF2",
    overFit:"auto"
    },
    error:{
        color:"red",
        fontSize:"13px"
    }
    
})


const AddBook = () => {
    const [book, setBook] = useState(initialValue);
    let [errorMessage, setErrorMessage] = useState("");
    let [desErrorMessage, setdesErrorMessage] = useState("");
    let [priceerrorMessage, setpriceErrorMessage] = useState("");
    let [discounterrorMessage, setdiscountErrorMessage] = useState("");
    let [availabilityerrorMessage, setavailabilityErrorMessage] = useState("");
    let [authorerrorMessage, setauthorErrorMessage] = useState("");
    let [publishererrorMessage, setpublisherErrorMessage] = useState("");
    let { bookname, book_description, book_price, book_discount ,book_availability,book_author,book_publisher} = book;
    let classes = useStyles();
    let history = useHistory();

    const addBookDetails = async() => {
        
      let ErrorMessage = validate_book_name(bookname)
      errorMessage=ErrorMessage
      if (errorMessage !== "Success") {
          setErrorMessage(errorMessage)
      }
  
  
      let deserrorMessage=validate_description(book_description)
      desErrorMessage=deserrorMessage;
      if(desErrorMessage!=="Success"){
          setdesErrorMessage(desErrorMessage)
      }


      let priceErrorMessage=validate_price(book_price)
      priceerrorMessage=priceErrorMessage;
      if(priceerrorMessage!=="Success"){
        setpriceErrorMessage(priceerrorMessage)
      }

      let discountErrorMessage=validate_discount(book_discount)
      discounterrorMessage=discountErrorMessage;
      if(discounterrorMessage!=="Success"){
        setdiscountErrorMessage(discounterrorMessage)
      }

      let avail =validate_stock(book_availability)
      availabilityerrorMessage=avail;
      if(availabilityerrorMessage!=="Success"){
        setavailabilityErrorMessage(availabilityerrorMessage)
      }

      let authorerror=validate_author_name(book_author)
      authorerrorMessage=authorerror;
      if(authorerrorMessage!=="Success"){
        setauthorErrorMessage(authorerrorMessage)
      }

      let pulishererror=validate_publisher_name(book_publisher)
      publishererrorMessage=pulishererror;
      if(publishererrorMessage!=="Success"){
        setpublisherErrorMessage(publishererrorMessage)
      }

      

      if(errorMessage==="Success" && deserrorMessage==="Success" && priceerrorMessage==="Success"
      && discounterrorMessage==="Success" && availabilityerrorMessage==="Success" &&
      authorerrorMessage==="Success" && publishererrorMessage==="Success"){     
            await addBook(book);
            toast.success('Book details added successfully', {
                position: toast.POSITION.TOP_RIGHT,autoClose:1000});
            history.push('./all');
        }
      else{
        alert("details are not valid")
      }
         
        
    }
    const addbookfield=(e)=>{
        
        setErrorMessage("")
        setBook({...book,bookname:e.target.value})    
    }
    const adddesfield=(e)=>{
        
        setdesErrorMessage("")
        setBook({...book,book_description:e.target.value})   
    }
    const addpricefield=(e)=>{
        
        setpriceErrorMessage("")
        setBook({...book,book_price:e.target.value}) 
    }
    const adddiscountfield=(e)=>{
       
        setdiscountErrorMessage("")
        setBook({...book,book_discount:e.target.value})     
    }
    const addavailfield=(e)=>{
    
        setavailabilityErrorMessage("")
        setBook({...book,book_availability:e.target.value}) 
    }
    const addauthorfield=(e)=>{
        
        setauthorErrorMessage("")
        setBook({...book,book_author:e.target.value}) 
    }
    const addpublisherfield=(e)=>{
       
        setpublisherErrorMessage("")
        setBook({...book,book_publisher:e.target.value}) 
    }

    return (
        
        <div className={classes.bg}>
            
            {/* <MediaCard/> */}
           
        <FormGroup className={classes.container} >
            <Typography variant="h4">Add Book</Typography>
            <FormControl xs={12} md={6} >
                <InputLabel htmlFor="my-input">Name</InputLabel>
                <Input
                onChange={addbookfield} name='bookname' value={book.bookname} id="my-input" />
                {errorMessage && <span className={classes.error}> {errorMessage} </span>}
                
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">Description</InputLabel>
                <Input onChange={adddesfield} name='description' value={book.book_description} id="my-input" />
                {desErrorMessage && <span className={classes.error}> {desErrorMessage} </span>}
                
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">price</InputLabel>
                <Input onChange={addpricefield} name='price' value={book.book_price} id="my-input"/>
                {priceerrorMessage && <p className={classes.error}> {priceerrorMessage} </p>}
                
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">discount</InputLabel>
                <Input onChange={adddiscountfield} name='discount' value={book.book_discount} id="my-input" />
                {discounterrorMessage && <p className={classes.error}> {discounterrorMessage} </p>}
                
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">availability</InputLabel>
                <Input onChange={addavailfield} name='availability' value={book.book_availability} id="my-input" />
                {availabilityerrorMessage && <p className={classes.error}> {availabilityerrorMessage} </p>}
                
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">author</InputLabel>
                <Input onChange={addauthorfield} name='author' value={book.book_author} id="my-input" />
                {authorerrorMessage && <p className={classes.error}> {authorerrorMessage} </p>}
                
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">publisher</InputLabel>
                <Input onChange={addpublisherfield} name='publisher' value={book.book_publisher} id="my-input" />
                {publishererrorMessage && <p className={classes.error}> {publishererrorMessage} </p>}
                
            </FormControl>
            <FormControl>
                <Button variant="contained" color="primary" onClick={() => addBookDetails()}>Add Book</Button>
                
            </FormControl>
            <br></br>
        </FormGroup>
        </div>
        
       
        
    )
}

export default AddBook;