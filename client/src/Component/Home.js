import { AppBar, Toolbar, makeStyles } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import MediaCard from './mediacard';
import { Paper } from '@material-ui/core';

const useStyle = makeStyles({
    header: {
        background: '#111111'
    },
    tabs: {
        color: '#FFFFFF',
        marginRight: 20,
        textDecoration: 'none',
        fontSize: 20
    },
    center:{
        // backgroundColor:"grey",
         backgroundImage: "url('https://media.geeksforgeeks.org/wp-content/uploads/rk.png')",
 
    },
    image:{
        // height:"610px",
        // width:"1010px"
        height:"100%",
        width:"100%"
    },
    container:{
        backgroundImage: "url('boook.jpg')",
        height:'100vh',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
        
      },
      
})

const Home = () => {
    const classes = useStyle();
    return (
        <>
        <div className={classes.container}>
        <div >
        {/* <MediaCard/> */}
        </div>
        <div className='center'>
            {/* <img src="boook.jpg" className={classes.image}>
                </img> */}
        </div>
        </div>
        </>
    )
}

export default Home;