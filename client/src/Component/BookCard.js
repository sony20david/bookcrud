import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { getBooks } from '../Service/api';
import { useState } from 'react';
import { useEffect } from 'react';
import { CardMedia } from '@material-ui/core';



const useStyles = makeStyles({
  root: {
    width:"300px",
    height:"400px",
    margin:"20px",
    backgroundColor:"#EFF5F5",
    overflowX:"auto",
    overflowY:"auto"
    
  },
  bgcontainer:{
    // display:"flex",
    // flexDirection:"row",
    // flexwrap:"wrap"
    display: "flex",
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    margin: 0,
    padding: 0,
    width:"100%"
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  cardcontent:{
      margin:"20px 0 20px 0",
      color:"black"
  },
  searchContainer: {
    marginTop: 20,
    display: "flex",
    justifyContent: "center",
    position:"static"
  },
  icon_rtl: {
    marginRight: 20,
    background:
      "url(https://static.thenounproject.com/png/101791-200.png) no-repeat right",
    backgroundSize: 20,
    backgroundColor: "white",
    width: 400,
    height:30,
    paddingLeft: 10,
  },
  notFound:{
     padding:"20"
  },
  image:{
    height:"40px",
    width:"50px",
    marginTop:"10px"
  },
  container:{
    display:"flex",
    flexDirection:"row",
    flexwrap:"wrap",
    overflowX:"auto",
    overflowY:"auto",
    backgroundColor:"#D7E7E6"
  },
  bg:{
    backgroundColor:"#D7E7E6",
  }
 
});

export default function BookCard() {
    const [books, setBooks] = useState([]);
    const [searchInput, setSearchInput] = useState("");
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  useEffect(() => {
    getAllBooks();
}, []);

  const getAllBooks = async () => {
    let response = await getBooks();
    setBooks(response.data);
  }
  const onChangeSearchInput = (event) => {
      setSearchInput(event.target.value);
      // console.log(event.target.value);
    };
    
    const searchResults = books.filter((book) =>
      book.bookname.toLowerCase().includes(searchInput)
    );
    console.log(searchResults)


  return (
    <>
    <div className={classes.container}>
      {/* <div>
        <MediaCard/>
      </div> */}
      
      <div className={classes.bg} xs={6}>
        <div className={classes.searchContainer}>
          <input
            className={classes.icon_rtl}
            placeholder="Search"
            type="search"
            onChange={onChangeSearchInput}
            value={searchInput}
          />
        </div>
        
      <div className={classes.bgcontainer}  >
        
    {searchResults.map((user) => (
     
    <Card className={classes.root} >

      <CardContent className={classes.cardcontent}>
  
        <Typography variant='h4' component="h6" > 
          {user.bookname}
        </Typography>
        <br></br>
        <Typography variant="body2" component="P">
            Book Description  :
          {user.book_description}
        </Typography>
        <br></br>
        <Typography  variant="body2" component="p">
            Book Price  :
          {user.book_price}
        </Typography>
        <br></br>
        <Typography variant="body2" component="p">
            Book Discount  :
         {user.book_discount}
        </Typography>
        <br></br>
        <Typography variant="body2" component="p">
            Book Availability :
         {user.book_availability}
        </Typography>
        <br></br>
        <Typography variant="body2" component="p">
            Book Author  :
         {user.book_author}
        </Typography>
        <br></br>
        <Typography variant="body2" component="p">
            Book Publisher  :
         {user.book_publisher}
        </Typography>
        

      </CardContent>
        </Card>
        ))}      
</div>
</div>

        </div>

        </>
  );
}
