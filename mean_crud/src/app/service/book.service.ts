import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BookService {


  constructor(private httpClient:HttpClient) { }

  getData(){
    return this.httpClient.get(environment.apiUrl)
  }

  insertData(data:any){
    return this.httpClient.post(environment.apiUrl+'/add',data)
  }

  getDataById(id:any){
    return this.httpClient.get(environment.apiUrl+id)
  }

  updateData(id:any,data:any){
    return this.httpClient.put(environment.apiUrl+id,data)
  }

  deleteData(id:any){
    return this.httpClient.delete(environment.apiUrl+id)
  }
}
