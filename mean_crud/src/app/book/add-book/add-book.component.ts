import { Component, OnInit } from '@angular/core';

import { BookService } from 'src/app/service/book.service';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';




@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  form!:FormGroup;
  // submitted!:false;
  submitted:boolean=false;
  data:any;
  msg:any;
  constructor(private bookService:BookService,private formBuilder:FormBuilder,private router:Router,private tostr:ToastrService) {
    
   }


   createForm(){
      const reg="^[a-zA-Z0-9 ]{8,30}$";

     this.form=this.formBuilder.group({
       bookname:['',[Validators.required,Validators.pattern(reg)]],
       book_description:['',[Validators.required,Validators.pattern(reg)]],
       book_price:['',Validators.required],
       book_discount:['',Validators.required],
       book_availability:['',Validators.required],
       book_author:['',Validators.required],
       book_publisher:['',Validators.required]
     })
   }

  ngOnInit(): void {
    this.createForm();
  }

  get f(){
    return this.form.controls;
   }
  insertData(){
    this.submitted=true;
    // alert(this.form.value.bookname)

    if(this.form.invalid){
      console.log("this form is in-valid");
      this.tostr.info("please fill all the details");
      
      return ;
    }
    
    else{
    this.bookService.insertData(this.form.value).subscribe(res=>{
      this.data=res;
      this.tostr.success("data saved successfully")
      this.router.navigateByUrl("/")
      
    })
    }
}

}
