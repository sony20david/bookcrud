import { Component, OnInit } from '@angular/core';

import { BookService } from 'src/app/service/book.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  books:any;
  data:any;
  constructor(private bookService:BookService,private toastr:ToastrService) { }

  ngOnInit(): void {
    this.getBooksData()
  }

  getBooksData(){
    this.bookService.getData().subscribe(res=>{
      console.log(res)
      this.books=res;
    })
  }

  deleteData(id:any){
    // this.books.splice(id-1,1)
    this.bookService.deleteData(id).subscribe(res=>{
      this.ngOnInit()
      this.data=res;
      this.toastr.error("deleted successfully")

    })
  }

}
